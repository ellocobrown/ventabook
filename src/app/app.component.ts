import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Libros } from './models/libros';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
// import {
//   Libros
// } from '../app/models';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // title = 'ventaBook';
  libroArray: Libros[] = [
    {id: 1, descripcion: "Descripcion Libro", titulo: "Titulo Libro", editorial: "Editorial Libro",autor: "hola", activo: 1}
  ];

   _form ;
  form: FormGroup;



  selectedLibro: Libros = new Libros();

  constructor(
    private http: HttpClient,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this._form = this.fb.group({
      titulo: ['', Validators.required],
      autor: ['', Validators.required]

    });


  }
  openEdit(libro: Libros){
    this.selectedLibro = libro;
  }

  addOrEdit() {
    // if (this.selectedLibro.id === 0) {
    //   this.selectedLibro.id = this.libroArray.length + 1;
    //   this.libroArray.push(this.selectedLibro);
    // }

    if(this._form.value.titulo || this._form.value.autor) {
    this.libroArray.push(this._form.value );
    let test = {libro: { titulo: this._form.value.titulo, autor: this._form.value.autor}};

    let data = JSON.stringify(test);

    return this.http.post<Libros[]>(`http://127.0.0.1:8000/api/libro/agregar`, data);
    }

  }

  delete() {
    if (confirm('¿Seguro que lo quieres eliminar?')) {
    this.libroArray = this.libroArray.filter(l => l != this.selectedLibro);
    this.selectedLibro = new Libros();
    }
  }
}
