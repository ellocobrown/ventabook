export class Libros {
    id: number = 0;
    descripcion: string;
    autor: string;
    titulo: string;
    editorial: string;
    activo: number;
}
